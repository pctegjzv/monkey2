FROM index.alauda.cn/alaudaorg/gobuild:1.10-alpine
WORKDIR $GOPATH
ARG COMMIT
ARG REPO
ARG TAG
COPY . src/monkey2
RUN cp -f src/monkey2/macvlan/macvlan.go src/monkey2/vendor/github.com/containernetworking/plugins/plugins/main/macvlan/macvlan.go && \
    CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -v -installsuffix cgo -o /macvlan monkey2/vendor/github.com/containernetworking/plugins/plugins/main/macvlan

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -v -installsuffix cgo -o /monkey-ipam monkey2/ipam && \
    CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -v -installsuffix cgo -o /monkey-recycle monkey2/recycle && \
    CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s" -v -installsuffix cgo -o /monkey-discovery monkey2/discovery && \
    CGO_ENABLED=0 GOOS=linux go build -ldflags "-w -s -X monkey2/version.RELEASE=${TAG} -X monkey2/version.COMMIT=${COMMIT} -X monkey2/version.REPO=${REPO}" -v -o /monkey-router monkey2/router && \
    upx /monkey-ipam && \
    upx /monkey-discovery && \
    upx /monkey-recycle && \
    upx /monkey-router && \
    upx /macvlan



FROM alpine:3.7
RUN sed -i 's|dl-cdn.alpinelinux.org|mirrors.aliyun.com|g' /etc/apk/repositories
RUN apk --no-cache add ca-certificates iptables ipset ipvsadm && update-ca-certificates
COPY discovery/nsenter /nsenter
COPY --from=0 monkey-ipam /monkey-ipam
COPY --from=0 monkey-router /monkey-router
COPY --from=0 monkey-recycle /monkey-recycle
COPY --from=0 monkey-discovery /monkey-discovery
COPY --from=0 macvlan /macvlan
COPY run.sh /run.sh
COPY monkey.conf /monkey.conf
CMD ["sh", "/run.sh"]

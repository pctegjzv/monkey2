.PHONY: build push version

REPO=$(shell git config --get remote.origin.url)
COMMIT := git-$(shell git rev-parse --short HEAD)
TAG?=0.1.1

build:
	gofmt -w ipam macvlan pkg router version recycle
	./hack/update-codegen.sh
	docker build --build-arg COMMIT=${COMMIT} --build-arg REPO=${REPO} --build-arg TAG=${TAG} -t index.alauda.cn/claas/monkey2:test .
	docker image prune -f
push-test: build
	docker push index.alauda.cn/claas/monkey2:test
push: build
	docker tag index.alauda.cn/claas/monkey2:test index.alauda.cn/claas/monkey2:${TAG}
	docker tag index.alauda.cn/claas/monkey2:test index.alauda.cn/claas/monkey2:${COMMIT}
	docker push index.alauda.cn/claas/monkey2:${TAG}
	docker push index.alauda.cn/claas/monkey2:${COMMIT}
version:
	echo ${REPO} ${COMMIT} ${TAG}

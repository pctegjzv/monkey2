package main

import (
	"fmt"
	"net"

	"github.com/golang/glog"
	"github.com/vishvananda/netlink"
)

func linkExists(name string) bool {
	_, err := netlink.LinkByName(name)
	if err != nil {
		return false
	}
	return true
}

func createMacvlanLink(name, master string) error {
	m, err := netlink.LinkByName(master)
	if err == nil {
		mv := &netlink.Macvlan{
			LinkAttrs: netlink.LinkAttrs{
				MTU:         m.Attrs().MTU,
				Name:        name,
				ParentIndex: m.Attrs().Index,
				Namespace:   m.Attrs().Namespace,
			},
			Mode: netlink.MACVLAN_MODE_BRIDGE,
		}

		if err := netlink.LinkAdd(mv); err != nil {
			return fmt.Errorf("failed to create macvlan: %v", err)
		}

		if err := netlink.LinkSetUp(mv); err != nil {
			return fmt.Errorf("failed to set %s up: %v", ROUTER_INTERFACE, err)
		}
		err = netlink.LinkSetARPOff(mv)
		if err != nil {
			glog.Errorf("disable %s arp off failed, %+v", name, err)
			return err
		}

	} else {
		glog.Errorf("failed to find master interface %s %v", master, err)
		return err
	}

	return nil
}

func createIpvlanLink(name, master string) error {
	m, err := netlink.LinkByName(master)
	if err == nil {
		mv := &netlink.IPVlan{
			LinkAttrs: netlink.LinkAttrs{
				MTU:         m.Attrs().MTU,
				Name:        name,
				ParentIndex: m.Attrs().Index,
				Namespace:   m.Attrs().Namespace,
			},
			Mode: netlink.IPVLAN_MODE_L2,
		}

		if err := netlink.LinkAdd(mv); err != nil {
			return fmt.Errorf("failed to create ipvlan: %v", err)
		}

		if err := netlink.LinkSetUp(mv); err != nil {
			return fmt.Errorf("failed to set %s up: %v", ROUTER_INTERFACE, err)
		}
		addr, err := getNicAddr(master)
		if err != nil {
			glog.Errorf("get master addr failed %v", err)
			return err
		}
		routeAddr := generateRouterAddr(addr)
		if err = netlink.AddrAdd(mv, &netlink.Addr{IPNet: routeAddr}); err != nil {
			fmt.Errorf("assign address to router nic failed %v", err)
			return err
		}
		err = netlink.LinkSetARPOff(mv)
		if err != nil {
			glog.Errorf("disable %s arp off failed, %+v", name, err)
			return err
		}

	} else {
		glog.Errorf("failed to find master interface %s %v", master, err)
		return err
	}

	return nil
}

func getRelatedRoutes() ([]netlink.Route, error) {
	m, err := netlink.LinkByName(ROUTER_INTERFACE)
	if err != nil {
		return nil, err
	}
	return netlink.RouteList(m, netlink.FAMILY_V4)
}

func checkRouteExist(route *netlink.Route) bool {
	routes, err := getRelatedRoutes()
	if err != nil {
		return false
	}
	for _, r := range routes {
		if r.Dst == route.Dst {
			return true
		}
	}
	return false
}

func addRoute(route *netlink.Route) error {
	err := netlink.RouteReplace(route)
	if err != nil {
		if checkRouteExist(route) {
			return nil
		} else {
			return err
		}
	}
	return nil
}

func delRoute(route *netlink.Route) error {
	err := netlink.RouteDel(route)
	if err != nil {
		if checkRouteExist(route) {
			return err
		} else {
			return nil
		}
	}
	return nil
}

func getNicAddr(name string) (*netlink.Addr, error) {
	m, err := netlink.LinkByName(name)
	if err != nil {
		return nil, err
	}

	addrs, _ := netlink.AddrList(m, netlink.FAMILY_V4)
	if len(addrs) == 0 {
		return nil, fmt.Errorf("no ip on nic %s", name)
	}
	return &addrs[0], nil
}

func generateRouterAddr(addr *netlink.Addr) *net.IPNet {
	ip := addr.IP
	mask := addr.Mask
	var broadcast []byte
	for i := range ip {
		broadcast = append(broadcast, ip[i]|(^mask[i]))
	}
	broadcast[len(broadcast)-1] = broadcast[len(broadcast)-1] - 1
	return &net.IPNet{IP: broadcast, Mask: net.IPMask{255, 255, 255, 255}}
}

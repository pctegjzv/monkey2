package main

import (
	"fmt"
	"github.com/golang/glog"
	"gopkg.in/fatih/set.v0"
	"os/exec"
	"strings"
)

const (
	ListRules  = "iptables -t nat -L POSTROUTING | grep SNAT | grep monkey | awk '{print $5}'"
	CreateRule = "iptables -t nat -A POSTROUTING -d %s/32 -j SNAT --to %s -m comment --comment \"monkey\""
	DeleteRule = "iptables -t nat -D POSTROUTING -d %s/32 -j SNAT --to %s -m comment --comment \"monkey\""
)

func getSnatRules() ([]string, error) {
	output, err := exec.Command("/bin/sh", "-c", ListRules).CombinedOutput()
	if err != nil {
		return nil, err
	}
	return strings.Split(string(output), "\n"), nil
}

func configureIptables(podIPs []string, routeIP string) error {
	existIPs, err := getSnatRules()
	if err != nil {
		return err
	}

	existIPSet := set.New()
	targetIPSet := set.New()
	for _, ip := range existIPs {
		if len(strings.TrimSpace(ip)) > 0 {
			existIPSet.Add(ip)
		}
	}
	for _, ip := range podIPs {
		targetIPSet.Add(ip)
	}

	toAddSet := set.Difference(targetIPSet, existIPSet)
	toDeleteSet := set.Difference(existIPSet, targetIPSet)

	for _, ip := range set.StringSlice(toAddSet) {
		output, err := exec.Command("/bin/sh", "-c", fmt.Sprintf(CreateRule, ip, routeIP)).CombinedOutput()
		if err != nil {
			glog.Errorf("create snat for %s failed %s", ip, output)
		}
	}

	for _, ip := range set.StringSlice(toDeleteSet) {
		output, err := exec.Command("/bin/sh", "-c", fmt.Sprintf(DeleteRule, ip, routeIP)).CombinedOutput()
		if err != nil {
			glog.Errorf("delete snat for %s failed %s", ip, output)
		}
	}
	return nil
}

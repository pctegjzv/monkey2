package main

import (
	"fmt"
	"os"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

var (
	MASTER_INTERFACE = "eth0"
	NETWORK_TYPE     = "macvlan"
	ROUTER_INTERFACE = "macvlan0"
	DRIVER           = "empty"
)

func getClientset() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.ContentType = "application/vnd.kubernetes.protobuf"
	config.Timeout = time.Duration(15 * time.Second)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	v, err := clientset.Discovery().ServerVersion()
	if err != nil {
		panic(err.Error())
	}

	fmt.Println(fmt.Sprintf("Running in Kubernetes Cluster version v%v.%v (%v) - git (%v) commit %v - platform %v",
		v.Major, v.Minor, v.GitVersion, v.GitTreeState, v.GitCommit, v.Platform))
	return clientset
}

func getEnvWithDefault(env string, defaultVal string) string {
	val := os.Getenv(env)
	if val == "" {
		val = defaultVal
	}
	return val
}

func init() {
	MASTER_INTERFACE = getEnvWithDefault("MASTER_INTERFACE", MASTER_INTERFACE)
	NETWORK_TYPE = getEnvWithDefault("NETWORK_TYPE", NETWORK_TYPE)
	if NETWORK_TYPE == "ipvlan" {
		ROUTER_INTERFACE = "ipvlan0"
	}
	DRIVER = getEnvWithDefault("DRIVER", DRIVER)
}

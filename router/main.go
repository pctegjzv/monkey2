package main

import (
	"flag"
	"fmt"
	"monkey2/version"
	"net"
	"os"
	"strings"
	"time"

	"github.com/golang/glog"
	"github.com/vishvananda/netlink"
	"gopkg.in/fatih/set.v0"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func main() {
	fmt.Println(version.String())

	flag.Parse()
	glog.MaxSize = 1024 * 1024 * 25
	defer glog.Flush()

	clientset := getClientset()

	for {
		time.Sleep(5 * time.Second)
		if !linkExists(ROUTER_INTERFACE) {
			glog.Infof("create router interface")
			if NETWORK_TYPE == "macvlan" {
				if err := createMacvlanLink(ROUTER_INTERFACE, MASTER_INTERFACE); err != nil {
					glog.Errorf("create router interface failed %v", err)
					continue
				}
			} else {
				if err := createIpvlanLink(ROUTER_INTERFACE, MASTER_INTERFACE); err != nil {
					glog.Errorf("create router interface failed %v", err)
					continue
				}
			}
		}

		hostname, _ := os.Hostname()
		pods, err := clientset.CoreV1().Pods("").List(metav1.ListOptions{})
		if err != nil {
			glog.Errorf("get pod list failed %v", err)
			continue
		}
		podIPSet := set.New()
		for _, pod := range pods.Items {
			if pod.Spec.NodeName == hostname && pod.Spec.HostNetwork == false {
				if len(strings.TrimSpace(pod.Status.PodIP)) > 0 {
					podIPSet.Add(pod.Status.PodIP)
				}
			}
		}

		if NETWORK_TYPE == "ipvlan" {
			addr, err := getNicAddr(MASTER_INTERFACE)
			if err != nil {
				glog.Errorf("get master addr failed %v", err)
				continue
			}
			routeAddr := generateRouterAddr(addr)
			configureIptables(set.StringSlice(podIPSet), routeAddr.IP.String())
		}

		existRoutes, err := getRelatedRoutes()
		if err != nil {
			glog.Errorf("failed to get exist route table %v", err)
			continue
		}

		routeIPSet := set.New()
		for _, route := range existRoutes {
			if !podIPSet.Has(route.Dst.IP.String()) {
				glog.Infof("delete route to %s", route.Dst.IP.String())
				err := delRoute(&route)
				if err != nil {
					glog.Errorf("delete route to %s failed, %+v", route.Dst.IP.String(), err)
				}
			} else {
				routeIPSet.Add(route.Dst.IP.String())
			}
		}

		toAdd := set.Difference(podIPSet, routeIPSet)
		r, _ := netlink.LinkByName(ROUTER_INTERFACE)
		for _, p := range toAdd.List() {
			podIP, _ := p.(string)
			glog.Infof("add %s to route", podIP)
			err := addRoute(&netlink.Route{
				LinkIndex: r.Attrs().Index,
				Scope:     netlink.SCOPE_LINK,
				Dst: &net.IPNet{
					IP:   net.ParseIP(podIP),
					Mask: net.IPv4Mask(255, 255, 255, 255),
				},
			})
			if err != nil {
				glog.Errorf("add ip %s to route failed %v", podIP, err)
				continue
			}
		}
		glog.Infof("wait next route sync")
		glog.Flush()
	}
}

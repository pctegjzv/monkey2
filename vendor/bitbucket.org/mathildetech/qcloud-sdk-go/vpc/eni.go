package vpc

const (
	DescribeNetworkInterfaces  = "DescribeNetworkInterfaces"
	AssignPrivateIpAddresses   = "AssignPrivateIpAddresses"
	UnassignPrivateIpAddresses = "UnassignPrivateIpAddresses"
	MigratePrivateIpAddress    = "MigratePrivateIpAddress"

	ErrNotAttached     = "InvalidNetworkInterfac.NotAttached"
	ErrInUse           = "InvalidPrivateIpAddress.InUse"
	ErrIpLimitExceeded = "InvalidNetworkInterface.IpLimitExceeded"
)

type NetworkInterfaceType struct {
	VpcId                 string
	SubnetId              string
	ZoneId                int
	EniName               string
	EniDescription        string
	NetworkInterfaceId    string
	Primary               bool
	MacAddress            string
	PrivateIpAddressesSet []*PrivateIpAddressesType
	InstanceSet           *InstanceType
	GroupSet              []*GroupType
}

type PrivateIpAddressesType struct {
	PrivateIpAddress string
	Primary          bool
	WanIp            string
	EipId            string
}

type InstanceType struct {
	InstanceId string
	AttachTime string
}

type GroupType struct {
	SgId   string
	SgName string
}

type DescribeNetworkInterfacesArgs struct {
	VpcId              string `ArgName:"vpcId"`
	NetworkInterfaceId string `ArgName:"networkInterfaceId"`
	EniName            string `ArgName:"eniName"`
	EniDescription     string `ArgName:"eniDescription"`
	InstanceId         string `ArgName:"instanceId"`
}

type DataType struct {
	Data []*NetworkInterfaceType
}

type DescribeNetworkInterfaceResponse struct {
	Code     int
	Message  string
	CodeDesc string
	Data     DataType
}

func (client *Client) DescribeNetworkInterfaces(args *DescribeNetworkInterfacesArgs) ([]*NetworkInterfaceType, error) {
	response := DescribeNetworkInterfaceResponse{}
	err := client.Invoke(DescribeNetworkInterfaces, args, &response)
	if err == nil {
		return response.Data.Data, nil
	}
	return nil, err
}

type AssignPrivateIpAddressArgs struct {
	VpcId              string `ArgName:"vpcId"`
	NetworkInterfaceId string `ArgName:"networkInterfaceId"`
	PrivateIpAddress   string `ArgName:"privateIpAddressSet.0.privateIpAddress"`
	Primary            bool   `ArgName:"privateIpAddressSet.0.primary"`
}

type CommonResponse struct {
	Code     int
	Message  string
	CodeDesc string
}

func (client *Client) AssignPrivateIpAddress(args *AssignPrivateIpAddressArgs) (*CommonResponse, error) {
	response := CommonResponse{}
	err := client.Invoke(AssignPrivateIpAddresses, args, &response)
	return &response, err
}

type UnassignPrivateIpAddressArgs struct {
	VpcId              string `ArgName:"vpcId"`
	NetworkInterfaceId string `ArgName:"networkInterfaceId"`
	PrivateIpAddress   string `ArgName:"privateIpAddress.0"`
}

func (client *Client) UnassignPrivateIpAddress(args *UnassignPrivateIpAddressArgs) (*CommonResponse, error) {
	response := CommonResponse{}
	err := client.Invoke(UnassignPrivateIpAddresses, args, &response)
	return &response, err
}

type MigratePrivateIpAddressArgs struct {
	VpcId                 string `ArgName:"vpcId"`
	PrivateIpAddress      string `ArgName:"privateIpAddress"`
	OldNetworkInterfaceId string `ArgName:"oldNetworkInterfaceId"`
	NewNetworkInterfaceId string `ArgName:"newNetworkInterfaceId"`
}

func (client *Client) MigratePrivateIpAddress(args *MigratePrivateIpAddressArgs) (*CommonResponse, error) {
	response := CommonResponse{}
	err := client.Invoke(MigratePrivateIpAddress, args, &response)
	return &response, err
}

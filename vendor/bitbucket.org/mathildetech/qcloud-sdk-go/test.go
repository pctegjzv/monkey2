package main

import (
	"log"

	"bitbucket.org/mathildetech/qcloud-sdk-go/cvm"
	"bitbucket.org/mathildetech/qcloud-sdk-go/common"
)

func main() {
	credential := common.Credential{
		SecretId: "YOUR_SECRET_ID",
		SecretKey: "YOUR_SECRET_KEY",
	}
	opts := common.Opts{
		Region: "ap-beijing",
	}
	client, err := cvm.NewClient(credential, opts)
	if err != nil {
		log.Fatal(err)
	}
	args := cvm.DescribeInstanceArgs{}
	cvms, err := client.DescribeInstances(&args)
	if err != nil {
		log.Fatal(cvms)
	}
	log.Println(cvms)

}

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced

// IP is a specification for a IP resource
type IP struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec IPSpec `json:"spec"`
}

// IPSpec is the spec for a IP resource
type IPSpec struct {
	Used  bool `json:"used"`
	Fresh bool `json:"fresh"`
	// Binded      bool   `json:"binded"`
	PodName     string `json:"pod_name,omitempty"`
	Namespace   string `json:"namespace,omitempty"`
	NodeName    string `json:"node_name,omitempty"`
	MacAddress  string `json:"mac_address,omitempty"`
	ContainerID string `json:"container_id,omitempty"`
	SubnetName  string `json:"subnet_name"`
	// for link alauda service
	ServiceID   string `json:"service_id"`
	ServiceName string `json:"service_name"`
	// ip affinity
	ControlledBy string `json:"controlled_by,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// IPList is a list of IP resources
type IPList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []IP `json:"items"`
}

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced

// Lock is a specification for a Lock resource
type Lock struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec LockSpec `json:"spec"`
}

// LockSpec is the spec for a Lock resource
type LockSpec struct {
	Owner string `json:"owner"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// LockList is a list of lock resources
type LockList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Lock `json:"items"`
}

// +genclient
// +genclient:noStatus
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
// +genclient:nonNamespaced

// Subnet is a specification for a Subnet resource
type Subnet struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec SubnetSpec `json:"spec"`
}

// SubnetSpec is the spec for a Subnet resource
type SubnetSpec struct {
	SubnetName  string `json:"subnet_name,omitempty"`
	Namespace   string `json:"namespace,omitempty"`
	ProjectName string `json:"project_name,omitempty"`
	CIDRBlock   string `json:"cidr_block,omitempty"`
	Gateway     string `json:"gateway,omitempty"`
	CreatedBy   string `json:"created_by,omitempty"`
	CreatedAt   string `json:"created_at,omitempty"`
	UpdatedAt   string `json:"update_at,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// SubnetList is a list of Subnet resources
type SubnetList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`

	Items []Subnet `json:"items"`
}

package main

import (
	"flag"
	"monkey2/ipam/config"
	"os"
	"strings"
	"time"

	"github.com/golang/glog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	SleepInterval = 5
	Timeout       = 35
)

func main() {
	flag.Parse()
	defer glog.Flush()
	for {
		time.Sleep(SleepInterval * time.Second)
		ch := make(chan struct{})

		go func() {
			alaudaClient := config.GetAlaudaClient()
			k8sClient := config.GetK8sClient()

			ipList, err := alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
			if err != nil {
				glog.Errorf("List ips error: %+v", err)
			}
			for _, ip := range ipList.Items {
				if ip.Spec.ControlledBy != "" && ip.Spec.Used == false {
					controllerBySlice := strings.Split(ip.Spec.ControlledBy, "/")
					kind, name := controllerBySlice[0], controllerBySlice[1]
					needRecycle := false
					switch kind {
					case "ReplicaSet":
						_, err := k8sClient.AppsV1beta2().ReplicaSets(ip.Spec.Namespace).Get(name, metav1.GetOptions{})
						if notExist(err) {
							needRecycle = true
						}
					case "Deployment":
						_, err := k8sClient.AppsV1beta2().Deployments(ip.Spec.Namespace).Get(name, metav1.GetOptions{})
						if notExist(err) {
							needRecycle = true
						}
					case "DaemonSet":
						_, err := k8sClient.AppsV1beta2().DaemonSets(ip.Spec.Namespace).Get(name, metav1.GetOptions{})
						if notExist(err) {
							needRecycle = true
						}
					case "StatefulSet":
						_, err := k8sClient.AppsV1beta2().StatefulSets(ip.Spec.Namespace).Get(name, metav1.GetOptions{})
						if notExist(err) {
							needRecycle = true
						}
					}
					if needRecycle {
						glog.Infof("will recycle ip %s, %+v", ip.Name, ip.Spec)
						ip.Spec.ServiceID = ""
						ip.Spec.ServiceName = ""
						ip.Spec.ControlledBy = ""
						ip.Spec.Namespace = ""
						ip.Spec.Fresh = true
						_, err = alaudaClient.AlaudaV1().IPs().Update(&ip)
						if err != nil {
							glog.Errorf("update ip failed, %+v", err)
						}
					}
				}
			}
			ch <- struct{}{}
			return
		}()

		select {
		case <-ch:
			glog.Info("continue next recycle")
		case <-time.After(Timeout * time.Second):
			glog.Error("recycle timeout")
			os.Exit(1)
		}
	}
}

func notExist(err error) bool {
	if err != nil {
		return strings.Contains(err.Error(), "not found")
	}
	return false
}

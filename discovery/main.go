package main

import (
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/golang/glog"
)

func main() {
	flag.Parse()
	glog.MaxSize = 1024 * 1024 * 25
	defer glog.Flush()

	for {
		time.Sleep(time.Duration(FLUSH_INTERVAL) * time.Second)
		glog.Flush()

		//if err := saveIpSet(); err != nil {
		//    continue
		//}
		//if err := saveIPVS(); err != nil {
		//    continue
		//}

		hostIptables, err := saveIptables("")
		if err != nil {
			glog.Errorf("enter host ns error %v", err)
			continue
		}
		f, err := os.Create("/tmp/iptables.tmp")
		if err != nil {
			glog.Errorf("create tmp iptables failed %v", err)
			continue
		}
		for _, line := range hostIptables {
			if _, err := f.WriteString(line + "\n"); err != nil {
				glog.Errorf("write iptables file failed %v", err)
				f.Close()
				continue
			}
		}
		f.Close()
		f.Sync()

		filepath.Walk(NETNS_PATH, func(path string, fi os.FileInfo, err error) error {
			if fi.IsDir() {
				return nil
			}
			if fi.Name() == "default" {
				return nil
			}
			nsPath := filepath.Join(NETNS_PATH, fi.Name())

			//if err = flushIpSet(filepath.Join(NETNS_PATH, fi.Name())); err != nil {
			//    return nil
			//}

			glog.Infof("try to enter %s", fi.Name())

			containerIptables, err := saveIptables(nsPath)
			if err != nil {
				return nil
			}

			diff := iptablesDifferent(hostIptables, containerIptables)
			if diff {
				glog.Infof("syncing iptables %v", diff)
				restoreIptables(nsPath)
			}

			return nil
		})
	}
}

// saveIptables return nat rules and remove nodeport rules
func saveIptables(netns string) ([]string, error) {
	var output string
	if netns == "" {
		raw, err := exec.Command("iptables-save", "-t", "nat").CombinedOutput()
		if err != nil {
			return nil, err
		}
		output = string(raw)
	} else {
		raw, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "iptables-save", "-t", "nat").CombinedOutput()
		if err != nil {
			glog.Errorf("enter ns %s failed %s %v", netns, raw, err)
			return nil, err
		}
		output = string(raw)
	}
	result := []string{}
	for _, line := range strings.Split(string(output), "\n") {
		if !strings.Contains(line, "NODEPORT") {
			result = append(result, line)
		}
	}
	return result, nil
}

func iptablesDifferent(hostIptables, containerIptables []string) bool {
	hi := trimIptables(hostIptables)
	ci := trimIptables(containerIptables)
	if len(hi) != len(ci) {
		glog.Infof("host iptables %i, container iptables %i", len(hi), len(ci))
		return true
	}
	for i := range hi {
		if hi[i] != ci[i] {
			glog.Infof(hi[i])
			glog.Infof(ci[i])
			return true
		}
	}
	return false
}

func trimIptables(raw []string) []string {
	result := []string{}
	for _, line := range raw {
		if strings.HasPrefix(line, ":") || strings.HasPrefix(line, "#") {
			continue
		}
		result = append(result, line)
	}
	sort.Strings(result)
	return result
}

func restoreIptables(netns string) {
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "iptables-restore", "-T", "nat", "/tmp/iptables.tmp").CombinedOutput()
	if err != nil {
		glog.Errorf("set iptables to %s failed %s %v", netns, output, err)
	}
}

func flushIpSet(netns string) error {
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "ipset", "flush", "-file", "/tmp/ipset.tmp").CombinedOutput()
	if err != nil {
		glog.Errorf("flush ipset to %s failed %s %v", netns, output, err)
		return err
	}
	return nil
}

func saveIpSet() error {
	output, err := exec.Command("ipset", "save").CombinedOutput()
	if err != nil {
		glog.Errorf("save ipset failed %s, %v", output, err)
		return err
	}
	f, err := os.Create("/tmp/ipset.tmp")
	if err != nil {
		glog.Errorf("failed to create tmp ipset %v", err)
		return err
	}
	defer f.Close()

	if _, err = f.Write(output); err != nil {
		glog.Errorf("write ipset to file failed %v", err)
		return err
	}
	f.Sync()
	return nil
}

func saveIPVS() error {
	output, err := exec.Command("ipvsadm-save").CombinedOutput()
	if err != nil {
		glog.Errorf("save ipvs failed %s, %v", output, err)
		return err
	}
	f, err := os.Create("/tmp/ipvs.tmp")
	if err != nil {
		glog.Errorf("failed to create tmp ipvs %v", err)
		return err
	}
	defer f.Close()

	if _, err = f.Write(output); err != nil {
		glog.Errorf("write ipvs to file failed %v", err)
		return err
	}
	f.Sync()
	return nil
}

func flushIPVS(netns string) error {
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "sh", "-c", "cat /tmp/ipvs.tmp | ipvsadm-restore").CombinedOutput()
	if err != nil {
		glog.Errorf("flush ipset to %s failed %s %v", netns, output, err)
		return err
	}
	return nil
}

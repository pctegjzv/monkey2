package main

import (
	alaudaclient "monkey2/pkg/client/clientset/versioned"
	"os"
	"time"

	"k8s.io/client-go/rest"
)

var (
	FLUSH_INTERVAL    = 5
	NETNS_PATH        = "/var/docker/netns"
	MACVLAN_INTERFACE = "macvlan0"
	DEFAULT_INTERFACE = getEnvWithDefault("MASTER_INTERFACE", "eth0")
	DEFAULT_NETNS     = "default"
)

func getEnvWithDefault(env string, defaultVal string) string {
	val := os.Getenv(env)
	if val == "" {
		val = defaultVal
	}
	return val
}

func GetAlaudaClient() *alaudaclient.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.Timeout = time.Duration(10 * time.Second)
	alaudaClient, err := alaudaclient.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return alaudaClient
}

#!/usr/bin/env sh
set -e

mkdir -p /var/log/mathilde

get_default_interface(){
    # default via 172.17.0.1 dev eth0
    # 172.17.0.0/16 dev eth0 scope link  src 172.17.0.2
    default_interface=$(ip route|grep default|cut -d " " -f 5)
    if [ ! -z "$default_interface" ]; then
        echo "$default_interface"
    else
	echo "eth0"
    fi
}

DEFAULT_INTERFACE=$(get_default_interface)
export MASTER_INTERFACE="${MASTER_INTERFACE:-$DEFAULT_INTERFACE}"

if [ ${MODE:=all} = "all" ]; then
    cp -f /monkey-ipam /opt/cni/bin/monkey
    cp -f /macvlan /opt/cni/bin/macvlan
    mkdir -p /host/var/run/secrets/kubernetes.io/serviceaccount
    cp /var/run/secrets/kubernetes.io/serviceaccount/..data/* /host/var/run/secrets/kubernetes.io/serviceaccount

    sed -i "s|{{NETWORK_TYPE}}|${NETWORK_TYPE:=macvlan}|g" /monkey.conf
    sed -i "s|{{KUBERNETES_SERVICE_HOST}}|${KUBERNETES_SERVICE_HOST}|g" /monkey.conf
    sed -i "s|{{KUBERNETES_SERVICE_PORT}}|${KUBERNETES_SERVICE_PORT}|g" /monkey.conf
    sed -i "s|{{MASTER_INTERFACE}}|${MASTER_INTERFACE}|g" /monkey.conf
    sed -i "s|{{DRIVER}}|${DRIVER:=empty}|g" /monkey.conf
    sed -i "s|{{REGION}}|${REGION}|g" /monkey.conf
    sed -i "s|{{SECRET_ID}}|${SECRET_ID}|g" /monkey.conf
    sed -i "s|{{SECRET_KEY}}|${SECRET_KEY}|g" /monkey.conf

    if [ ${MODE:=all} = "all" ]; then
        cp -f /monkey-ipam /opt/cni/bin/monkey
        cp -f /macvlan /opt/cni/bin/macvlan
        rm -rf /etc/cni/net.d/*
        cp /monkey.conf /etc/cni/net.d/monkey.conf
    fi

    mv /nsenter /usr/bin/nsenter

    /monkey-router -log_dir=/var/log/mathilde/ -stderrthreshold=INFO
elif [ ${MODE} = "route" ]; then
    cp -f /monkey-ipam /opt/cni/bin/monkey
    cp -f /macvlan /opt/cni/bin/macvlan
    mkdir -p /host/var/run/secrets/kubernetes.io/serviceaccount
    cp /var/run/secrets/kubernetes.io/serviceaccount/..data/* /host/var/run/secrets/kubernetes.io/serviceaccount

    sed -i "s|{{NETWORK_TYPE}}|${NETWORK_TYPE:=macvlan}|g" /monkey.conf
    sed -i "s|{{KUBERNETES_SERVICE_HOST}}|${KUBERNETES_SERVICE_HOST}|g" /monkey.conf
    sed -i "s|{{KUBERNETES_SERVICE_PORT}}|${KUBERNETES_SERVICE_PORT}|g" /monkey.conf
    sed -i "s|{{MASTER_INTERFACE}}|${MASTER_INTERFACE}|g" /monkey.conf
    sed -i "s|{{DRIVER}}|${DRIVER:=empty}|g" /monkey.conf
    sed -i "s|{{REGION}}|${REGION}|g" /monkey.conf
    sed -i "s|{{SECRET_ID}}|${SECRET_ID}|g" /monkey.conf
    sed -i "s|{{SECRET_KEY}}|${SECRET_KEY}|g" /monkey.conf

    if [ ${MODE:=all} = "all" ]; then
        cp -f /monkey-ipam /opt/cni/bin/monkey
        cp -f /macvlan /opt/cni/bin/macvlan
        rm -rf /etc/cni/net.d/*
        cp /monkey.conf /etc/cni/net.d/monkey.conf
    fi

    mv /nsenter /usr/bin/nsenter

    /monkey-router -log_dir=/var/log/mathilde/ -stderrthreshold=INFO
elif [ ${MODE} = "discovery" ]; then
    mv /nsenter /usr/bin/nsenter
    /monkey-discovery -stderrthreshold=INFO
fi

package main

import (
	"log"
	"monkey2/ipam/config"
	"os"

	"github.com/containernetworking/cni/pkg/skel"
	"github.com/containernetworking/cni/pkg/types"
	"github.com/containernetworking/cni/pkg/version"
)

// LOGPATH monkey log path, need rotation
const LOGPATH = "/var/log/mathilde/monkey_cni.log"

func main() {
	skel.PluginMain(cmdAdd, cmdDel, version.All)
}

func cmdAdd(args *skel.CmdArgs) error {
	ipamConf, confVersion, err := config.LoadIPAMConfig(args.StdinData, args.Args)
	ipamConf.ContainerID = args.ContainerID
	if err != nil {
		return err
	}

	f, err := os.OpenFile(LOGPATH, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		f = nil
	} else {
		defer f.Close()
	}
	logger := log.New(f, "IPAM-ADD: ", log.LstdFlags|log.Lshortfile)
	ipAllocator, err := NewIPAllocator(ipamConf, logger)
	if err != nil {
		return err
	}
	result, err := ipAllocator.Acquire()
	if err != nil {
		logger.Printf("cmdadd error: %v\n", err)
		return err
	}
	logger.Printf("add result: %+v\n", result)
	// write arp
	logger.Printf("will write arp for pod %s\n", ipamConf.PodName)
	podMAC, err := getMACWithNs(args.Netns, "eth0")
	if err != nil {
		logger.Printf("get pod mac error: %v\n", err)
		return err
	}
	podIP := result.IPs[0].Address.IP.String()
	hostMAC, err := getMAC(config.MACVLAN_INTERFACE)
	if err != nil {
		logger.Printf("get host mac error: %v\n", err)
	}
	hostIP, err := getIP(config.DEFAULT_INTERFACE)
	if err != nil {
		logger.Printf("get host ip error: %v\n", err)
	}
	logger.Printf("add to pod arp, ns:%s, if:%s, ip:%s, mac:%s\n", args.Netns, "eth0", hostIP, hostMAC)
	err = addStaticARPWithNs(args.Netns, "eth0", hostIP, hostMAC)
	if err != nil {
		logger.Printf("add static arp to pod error: %v\n", err)
	}
	logger.Printf("add to host arp, if:%s, ip:%s, mac:%s\n", config.MACVLAN_INTERFACE, podIP, podMAC)
	err = addStaticARP(config.MACVLAN_INTERFACE, podIP, podMAC)
	if err != nil {
		logger.Printf("add static arp to host error: %v\n", err)
	}
	return types.PrintResult(result, confVersion)
}

func cmdDel(args *skel.CmdArgs) error {
	ipamConf, _, err := config.LoadIPAMConfig(args.StdinData, args.Args)
	ipamConf.ContainerID = args.ContainerID
	if err != nil {
		return err
	}
	f, err := os.OpenFile(LOGPATH, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		f = nil
	} else {
		defer f.Close()
	}
	logger := log.New(f, "IPAM-DEL: ", log.LstdFlags|log.Lshortfile)
	ipAllocator, err := NewIPAllocator(ipamConf, logger)
	if err != nil {
		return err
	}

	// delete arp
	logger.Printf("will delete arp for pod %s\n", ipamConf.PodName)
	podIP, err := getIPWithNs(args.Netns, "eth0")
	if err != nil {
		logger.Printf("get pod ip failed: %v\n", err)
	} else if podIP != "" {
		logger.Printf("delete from host arp, if:%s, ip:%s\n", config.MACVLAN_INTERFACE, podIP)
		err := delStaticARP(config.MACVLAN_INTERFACE, podIP)
		if err != nil {
			logger.Printf("delete static arp failed, err: %v\n", err)
		}
	}

	err = ipAllocator.Release()
	if err != nil {
		logger.Printf("cmddel error: %v\n", err)
		return err
	}
	return nil
}

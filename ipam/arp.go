package main

import (
	"errors"
	"fmt"
	"os/exec"
	"regexp"

	"github.com/golang/glog"
)

var (
	IPRegex  = regexp.MustCompile(`(?m).*inet ([0-9.]+)/`)
	MACRegex = regexp.MustCompile(`(?m).*ether ([0-9a-zA-Z:]+)\s`)
)

type IPMAC struct {
	IP  string
	MAC string
}

func addStaticARP(iface, ip, mac string) error {
	glog.Infof("will add static arp [%s %s], iface %s", ip, mac, iface)
	output, err := exec.Command("arp", "-i", iface, "-s", ip, mac).CombinedOutput()
	if err != nil {
		glog.Errorf("add static arp [%s %s] failed, %s, err: %+v", ip, mac, string(output), err)
		return err
	}
	return nil
}

func delStaticARP(iface, ip string) error {
	glog.Infof("will delete static arp [%s], iface %s", ip, iface)
	output, err := exec.Command("arp", "-i", iface, "-d", ip).CombinedOutput()
	if err != nil {
		glog.Errorf("delete static arp [%s] failed, %s, err: %+v", ip, string(output), err)
		return err
	}
	return nil
}

func addStaticARPWithNs(netns, iface, ip, mac string) error {
	glog.Infof("will add static arp [%s %s] to ns %s, iface %s", ip, mac, netns, iface)
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "arp", "-i", iface, "-s", ip, mac).CombinedOutput()
	if err != nil {
		glog.Errorf("add static arp [%s %s] to ns: %s failed, %s, err: %+v", ip, mac, netns, string(output), err)
		return err
	}
	return nil
}

func delStaticARPWithNs(netns, iface, ip string) error {
	glog.Infof("will delete static arp [%s] from ns %s, iface %s", ip, netns, iface)
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "arp", "-i", iface, "-d", ip).CombinedOutput()
	if err != nil {
		glog.Errorf("delete static arp [%s] rom ns: %s failed, %s, err: %+v", ip, netns, string(output), err)
		return err
	}
	return nil
}

func getIP(interfaceName string) (string, error) {
	output, err := exec.Command("ip", "-f", "inet", "addr", "show", interfaceName).CombinedOutput()
	if err != nil {
		glog.Errorf("get ip failed, %s, err: %+v", string(output), err)
		return "", err
	}
	result := IPRegex.FindSubmatch(output)
	if len(result) > 0 {
		return string(result[1]), nil
	}
	return "", errors.New("get ip failed, no result")
}

func getMAC(interfaceName string) (string, error) {
	output, err := exec.Command("ip", "-f", "link", "addr", "show", interfaceName).CombinedOutput()
	if err != nil {
		glog.Errorf("get mac failed, %s, err: %+v", string(output), err)
		return "", err
	}
	result := MACRegex.FindSubmatch(output)
	if len(result) > 0 {
		return string(result[1]), nil
	}
	return "", errors.New("get mac failed, no result")
}

func getIPWithNs(netns, interfaceName string) (string, error) {
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "ip", "-f", "inet", "addr", "show", interfaceName).CombinedOutput()
	if err != nil {
		glog.Errorf("get ip failed, %s, err: %+v", string(output), err)
		return "", err
	}
	result := IPRegex.FindSubmatch(output)
	if len(result) > 0 {
		return string(result[1]), nil
	}
	return "", errors.New("get ip failed, no result")
}

func getMACWithNs(netns, interfaceName string) (string, error) {
	output, err := exec.Command("nsenter", fmt.Sprintf("--net=%s", netns), "ip", "-f", "link", "addr", "show", interfaceName).CombinedOutput()
	if err != nil {
		glog.Errorf("get mac failed, %s, err: %+v", string(output), err)
		return "", err
	}
	result := MACRegex.FindSubmatch(output)
	if len(result) > 0 {
		return string(result[1]), nil
	}
	return "", errors.New("get mac failed, no result")
}

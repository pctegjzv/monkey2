package config

import (
	"encoding/json"
	"errors"
	"fmt"
	alaudaclient "monkey2/pkg/client/clientset/versioned"
	"os"
	"strings"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

var (
	MACVLAN_INTERFACE = "macvlan0"
	DEFAULT_INTERFACE = getEnvWithDefault("MASTER_INTERFACE", "eth0")
)

// IPAMConfig represents the IP related network configuration.
type IPAMConfig struct {
	Name                  string
	PodName               string
	PodNamespace          string
	ContainerID           string
	KubernetesServiceHost string `json:"kubernetes_service_host"`
	KubernetesServicePort string `json:"kubernetes_service_port"`
	Driver                string `json:"driver"`
	Region                string `json:"region"`
	SecretId              string `json:"secret_id"`
	SecretKey             string `json:"secret_key"`
}

type CNIConfig struct {
	Name       string      `json:"name"`
	CNIVersion string      `json:"cniVersion"`
	IPAM       *IPAMConfig `json:"ipam"`
}

// LoadIPAMConfig creates an IPAMConfig from /etc/cni/net.d/monkey.conf
func LoadIPAMConfig(bytes []byte, args string) (*IPAMConfig, string, error) {
	n := CNIConfig{}
	if err := json.Unmarshal(bytes, &n); err != nil {
		return nil, "", err
	}

	if n.IPAM == nil {
		return nil, "", fmt.Errorf("CNI config missing 'ipam' key")
	}

	podName, err := parseValueFromArgs("K8S_POD_NAME", args)
	if err != nil {
		return nil, "", err
	}
	podNamespace, err := parseValueFromArgs("K8S_POD_NAMESPACE", args)
	if err != nil {
		return nil, "", err
	}
	n.IPAM.PodName = podName
	n.IPAM.PodNamespace = podNamespace

	os.Setenv("KUBERNETES_SERVICE_HOST", n.IPAM.KubernetesServiceHost)
	os.Setenv("KUBERNETES_SERVICE_PORT", n.IPAM.KubernetesServicePort)

	return n.IPAM, n.CNIVersion, nil
}

func parseValueFromArgs(key, argString string) (string, error) {
	if argString == "" {
		return "", errors.New("CNI_ARGS is required")
	}
	args := strings.Split(argString, ";")
	for _, arg := range args {
		if strings.HasPrefix(arg, fmt.Sprintf("%s=", key)) {
			podName := strings.TrimPrefix(arg, fmt.Sprintf("%s=", key))
			if len(podName) > 0 {
				return podName, nil
			}
		}
	}
	return "", fmt.Errorf("%s is required in CNI_ARGS", key)
}

func GetAlaudaClient() *alaudaclient.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.Timeout = time.Duration(10 * time.Second)
	alaudaClient, err := alaudaclient.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return alaudaClient
}

func GetK8sClient() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.ContentType = "application/vnd.kubernetes.protobuf"
	config.Timeout = time.Duration(10 * time.Second)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return clientset
}

func getEnvWithDefault(env string, defaultVal string) string {
	val := os.Getenv(env)
	if val == "" {
		val = defaultVal
	}
	return val
}

package driver

type EmptyDriver struct{}

func (e EmptyDriver) AssignPrivateAddress(privateAddress string) error {
	return nil
}

func (e EmptyDriver) UnassignPrivateAddress(privateAddress string) error {
	return nil
}

package driver

import (
	"fmt"
	"monkey2/ipam/config"
)

type IPAMDriver interface {
	AssignPrivateAddress(privateAddress string) error
	UnassignPrivateAddress(privateAddress string) error
}

func GetIPAMDriver(driverType string, IPAMConfig *config.IPAMConfig) (IPAMDriver, error) {
	switch driverType {
	case "empty":
		return EmptyDriver{}, nil
	case "qcloud":
		return QcloudDriver{Region: IPAMConfig.Region, SecretId: IPAMConfig.SecretId, SecretKey: IPAMConfig.SecretKey}, nil
	default:
		return nil, fmt.Errorf("%s is not a supported ipam driver", driverType)
	}
}

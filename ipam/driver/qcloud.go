package driver

import (
	"bitbucket.org/mathildetech/qcloud-sdk-go/common"
	"bitbucket.org/mathildetech/qcloud-sdk-go/vpc"
	"fmt"
	"github.com/parnurzeal/gorequest"
	"math/rand"
	"time"
)

var (
	request = gorequest.New().Timeout(5 * time.Second)
)

const (
	InstanceIdMetaURL = "http://metadata.tencentyun.com/meta-data/instance-id"
	MaxRetry          = 5
)

type QcloudDriver struct {
	Region    string
	SecretId  string
	SecretKey string
}

func (q QcloudDriver) AssignPrivateAddress(privateAddress string) error {
	instanceId, err := getInstanceId()
	if err != nil {
		return fmt.Errorf("get instance id from qcloud meta failed %v", err)
	}

	//TODO: only support assign private ip for primary nic for simplicity
	client := q.getQcloudClient()
	primaryNic, err := getPrimaryNetworkInterface(client, instanceId)
	if err != nil {
		return err
	}
	for retry := 0; retry < MaxRetry; retry++ {

		// check if address already used and migrate
		oldNic, err := getNetWorkInterfaceByIp(client, privateAddress, primaryNic.VpcId)
		if err != nil {
			return err
		}
		if oldNic != nil {
			if oldNic.NetworkInterfaceId == primaryNic.NetworkInterfaceId {
				// ip already bind to this nic
				return nil
			}
			res, err := client.MigratePrivateIpAddress(&vpc.MigratePrivateIpAddressArgs{
				VpcId:                 primaryNic.VpcId,
				PrivateIpAddress:      privateAddress,
				OldNetworkInterfaceId: oldNic.NetworkInterfaceId,
				NewNetworkInterfaceId: primaryNic.NetworkInterfaceId,
			})
			if err != nil && responseEmpty(res) {
				return err
			}
			if res.CodeDesc == vpc.ErrIpLimitExceeded {
				//TODO: we should delete the pod to force a redeploy and prey it will move to another node.
				return fmt.Errorf("exceed nic ip limit")
			}
			if res.Code == 0 {
				return nil
			}
		} else {
			// address not exist assign it
			res, err := client.AssignPrivateIpAddress(&vpc.AssignPrivateIpAddressArgs{
				VpcId:              primaryNic.VpcId,
				NetworkInterfaceId: primaryNic.NetworkInterfaceId,
				PrivateIpAddress:   privateAddress,
				Primary:            false})
			if err != nil && responseEmpty(res) {
				return err
			}
			if res.Code != 0 {
				if res.CodeDesc == vpc.ErrIpLimitExceeded {
					//TODO: we should delete the pod to force a redeploy and prey it will move to another node.
					return fmt.Errorf("exceed nic ip limit")
				}
			} else {
				return nil
			}
		}

		// random sleep to wait conflict task finish
		r := rand.Intn(10)
		time.Sleep(time.Duration(r+1) * 500 * time.Microsecond)
	}

	return fmt.Errorf("retry %d times and failed %v", MaxRetry, err)
}

func (q QcloudDriver) UnassignPrivateAddress(privateAddress string) error {
	instanceId, err := getInstanceId()
	if err != nil {
		return fmt.Errorf("get instance id from qcloud meta failed %v", err)
	}
	client := q.getQcloudClient()
	primaryNic, err := getPrimaryNetworkInterface(client, instanceId)
	for retry := 0; retry < MaxRetry; retry++ {
		ipExist := false
		for _, ip := range primaryNic.PrivateIpAddressesSet {
			if ip.PrivateIpAddress == privateAddress {
				ipExist = true
			}
		}
		if ipExist {
			res, err := client.UnassignPrivateIpAddress(&vpc.UnassignPrivateIpAddressArgs{
				PrivateIpAddress:   privateAddress,
				NetworkInterfaceId: primaryNic.NetworkInterfaceId,
				VpcId:              primaryNic.VpcId})
			if err != nil && responseEmpty(res) {
				return err
			}
			if err == nil {
				// wait for unassign finished
				time.Sleep(500 * time.Microsecond)
				return nil
			}
		} else {
			// ip already not bind to this nic
			return nil
		}

		// random sleep to wait conflict task finish
		r := rand.Intn(10)
		time.Sleep(time.Duration(r+1) * 500 * time.Microsecond)
	}
	return nil
}

func (q QcloudDriver) getQcloudClient() *vpc.Client {
	credential := common.Credential{
		SecretId:  q.SecretId,
		SecretKey: q.SecretKey,
	}
	opts := common.Opts{
		Region: q.Region,
	}
	client, _ := vpc.NewClient(credential, opts)
	return client
}

func getInstanceId() (string, error) {
	response, body, errs := request.Get(InstanceIdMetaURL).End()
	if len(errs) != 0 {
		return "", errs[0]
	}
	if response.StatusCode != 200 {
		return "", fmt.Errorf(body)
	}
	return body, nil
}

func getPrimaryNetworkInterface(client *vpc.Client, instanceId string) (*vpc.NetworkInterfaceType, error) {
	nics, err := client.DescribeNetworkInterfaces(&vpc.DescribeNetworkInterfacesArgs{InstanceId: instanceId})
	if err != nil {
		return nil, fmt.Errorf("get nic of instance %s from qcloud failed %v", instanceId, err)
	}
	if len(nics) == 0 {
		return nil, fmt.Errorf("get zero nic from %s", instanceId)
	}
	for _, nic := range nics {
		if nic.Primary {
			return nic, nil
		}
	}
	return nil, fmt.Errorf("no primary networkinterface for instance %s", instanceId)
}

func getNetWorkInterfaceByIp(client *vpc.Client, privateAddress, vpcId string) (*vpc.NetworkInterfaceType, error) {
	nics, err := client.DescribeNetworkInterfaces(&vpc.DescribeNetworkInterfacesArgs{VpcId: vpcId})
	if err != nil {
		return nil, fmt.Errorf("get nic of address %s from qcloud failed %v", privateAddress, err)
	}
	if len(nics) == 0 {
		return nil, nil
	}
	for _, nic := range nics {
		for _, ip := range nic.PrivateIpAddressesSet {
			if ip.PrivateIpAddress == privateAddress {
				//TODO: if ip already occupied by another nic as primary, we now have to manually delete the ip and pod to force a redeploy
				if ip.Primary {
					return nil, fmt.Errorf("address %s is primary address of nic %s can not be transfered", ip.PrivateIpAddress, nic.NetworkInterfaceId)
				}
				return nic, nil
			}
		}
	}
	return nil, nil
}

func responseEmpty(response *vpc.CommonResponse) bool {
	return response.Code == 0 && response.CodeDesc == "" && response.Message == ""
}

package main

import (
	"fmt"
	"monkey2/ipam/config"
	"monkey2/ipam/driver"
	"monkey2/pkg/apis/alauda/v1"
	"monkey2/pkg/client/clientset/versioned"
	"net"
	"os"
	"strings"
	"time"

	"log"

	"github.com/containernetworking/cni/pkg/types"
	"github.com/containernetworking/cni/pkg/types/current"
	"github.com/pkg/errors"
	"gopkg.in/fatih/set.v0"
	k8v1 "k8s.io/api/core/v1"
	k8s_error "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	RequiredAddresses = "subnet.alauda.io/ipAddrs"
	RequiredSubnet    = "subnet.alauda.io/name"
	BindServiceID     = "service.alauda.io/uuid"
	BindServiceName   = "service.alauda.io/name"
)

type IPAllocator struct {
	Conf   *config.IPAMConfig
	Logger *log.Logger
}

type IPConfig struct {
	Address string `json:"address"`
	CIDR    string `json:"cidr"`
	Gateway string `json:"gateway"`
}

func NewIPAllocator(conf *config.IPAMConfig, logger *log.Logger) (*IPAllocator, error) {
	return &IPAllocator{Conf: conf, Logger: logger}, nil
}

func (alloc *IPAllocator) Acquire() (*current.Result, error) {
	alaudaClient := config.GetAlaudaClient()
	allIPs, err := alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	if len(allIPs.Items) == 0 {
		return nil, errors.New("no ip resource exists, please create ips before create app")
	}

	// find ip that already allocated to this pod
	for _, ip := range allIPs.Items {
		if ip.Spec.PodName == alloc.Conf.PodName && ip.Spec.Namespace == alloc.Conf.PodNamespace && ip.Spec.ContainerID == alloc.Conf.ContainerID {
			d, err := driver.GetIPAMDriver(alloc.Conf.Driver, alloc.Conf)
			if err != nil {
				return nil, err
			}
			err = d.AssignPrivateAddress(ip.Name)
			if err != nil {
				return nil, err
			}
			subnet, err := getSubnet(ip.Spec.SubnetName)
			if err != nil {
				return nil, err
			}
			if subnet == nil {
				return nil, errors.New("cannot find associated subnet")
			}
			return generateResult(IPConfig{
				Address: ip.Name,
				CIDR:    subnet.Spec.CIDRBlock,
				Gateway: subnet.Spec.Gateway,
			}), nil
		}
	}
	pod, err := getPod(alloc.Conf.PodName, alloc.Conf.PodNamespace)
	if err != nil {
		return nil, err
	}
	// serviceID and serviceName could be ""
	serviceID := pod.Labels[BindServiceID]
	serviceName := pod.Labels[BindServiceName]
	controlledBy := getPodControlledBy(pod)

	// bind those ip with service
	specificIPs := getPodRequiredIPs(pod)
	if len(specificIPs) > 0 {
		alloc.Logger.Printf("will bind ip with service: %s\n", serviceName)
		for _, sip := range specificIPs {
			for _, ip := range allIPs.Items {
				if sip == ip.Name {
					if ip.Spec.ControlledBy != "" && ip.Spec.ControlledBy != controlledBy {
						alloc.Logger.Printf("ip: %s already binded other service: %s, conflict\n", sip, controlledBy)
						continue
					}
					if ip.Spec.Used == false && ip.Spec.ControlledBy == "" {
						ip.Spec.ServiceID = serviceID
						ip.Spec.ServiceName = serviceName
						ip.Spec.ControlledBy = controlledBy
						ip.Spec.Namespace = pod.Namespace
						_, err := alaudaClient.AlaudaV1().IPs().Update(&ip)
						if err != nil {
							alloc.Logger.Printf("update ip %s failed %v, %s", ip.Name, err, ip.ObjectMeta.ResourceVersion)
						}
						alloc.Logger.Printf("bind ip: %s with service: %s\n", ip.Name, serviceName)
					}
				}
			}
		}
	}

	requiredAddresses, err := findPodRequiredAddresses(pod, alloc.Logger)
	if err != nil {
		return nil, fmt.Errorf("get pod %s required addresses failed %v", alloc.Conf.PodName, err)
	}
	alloc.Logger.Printf("required addresses for pod: %s, %+v\n", alloc.Conf.PodName, requiredAddresses)

	// refetch ip list, avoid conflict
	allIPs, err = alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	var allocSpecificIPErr error
	// have specific ip requirement
	if requiredAddresses != nil && len(requiredAddresses) > 0 {
		for _, requiredIP := range requiredAddresses {
			for _, ip := range allIPs.Items {
				if ip.Name == requiredIP {
					subnet, err := getSubnet(ip.Spec.SubnetName)
					if err != nil {
						return nil, err
					}
					err = alloc.acquire(alaudaClient, ip, serviceID, serviceName, controlledBy)
					if err != nil {
						if err.Error() == "conflict" {
							alloc.Logger.Printf("ip: %s conflict, find next\n", ip.Name)
							allocSpecificIPErr = err
							continue
						}
						return nil, err
					}
					return generateResult(IPConfig{
						Address: ip.Name,
						CIDR:    subnet.Spec.CIDRBlock,
						Gateway: subnet.Spec.Gateway,
					}), nil
				}
			}
		}
	}
	if allocSpecificIPErr != nil {
		// pod specific address alloc failed, dont alloc ip from all available ips
		return nil, fmt.Errorf("no ip available for pod %s", alloc.Conf.PodName)
	}

	// find available ip and acquire it
	for _, ip := range allIPs.Items {
		if ip.Spec.Used == false && (ip.Spec.ControlledBy == "" || ip.Spec.ControlledBy == controlledBy) {
			err := alloc.acquire(alaudaClient, ip, serviceID, serviceName, controlledBy)
			if err != nil {
				if err.Error() == "conflict" {
					alloc.Logger.Printf("ip: %s conflict, find next\n", ip.Name)
					continue
				}
				return nil, err
			} else {
				subnet, err := getSubnet(ip.Spec.SubnetName)
				if err != nil {
					return nil, err
				}
				return generateResult(IPConfig{
					Address: ip.Name,
					CIDR:    subnet.Spec.CIDRBlock,
					Gateway: subnet.Spec.Gateway,
				}), nil
			}
		}
	}

	// no ip available for this pod
	err = alloc.gc()
	if err != nil {
		alloc.Logger.Printf("gc failed, err: %v", err)
	}
	return nil, fmt.Errorf("no ip available for pod %s", alloc.Conf.PodName)
}

func generateResult(config IPConfig) *current.Result {
	result := current.Result{}

	ip := current.IPConfig{}
	ip.Version = "4"
	_, mask, _ := net.ParseCIDR(config.CIDR)
	ip.Address = net.IPNet{IP: net.ParseIP(config.Address).To4(), Mask: mask.Mask}
	ip.Gateway = net.ParseIP(config.Gateway).To4()
	result.IPs = []*current.IPConfig{&ip}

	route := types.Route{}
	route.Dst = net.IPNet{IP: net.ParseIP("0.0.0.0").To4(), Mask: net.CIDRMask(0, 32)}
	route.GW = net.ParseIP(config.Gateway).To4()
	result.Routes = []*types.Route{&route}
	return &result
}

func (alloc *IPAllocator) acquire(client *versioned.Clientset, ip v1.IP, serviceID, serviceName, controlledBy string) error {
	lock, err := client.AlaudaV1().Locks().Get(ip.Name, metav1.GetOptions{})
	if err == nil {
		if lock.Spec.Owner == fmt.Sprintf("%s-%s", alloc.Conf.PodNamespace, alloc.Conf.PodName) {
			return nil
		}
		return fmt.Errorf("ip %s already locked by %s-%s", ip.Name, alloc.Conf.PodNamespace, alloc.Conf.PodName)
	}
	lock, err = client.AlaudaV1().Locks().Create(&v1.Lock{ObjectMeta: metav1.ObjectMeta{Name: ip.Name}, Spec: v1.LockSpec{Owner: fmt.Sprintf("%s-%s", alloc.Conf.PodNamespace, alloc.Conf.PodName)}})
	if err != nil {
		if k8s_error.IsConflict(err) {
			return fmt.Errorf("ip %s already locked", ip.Name)
		}
		return fmt.Errorf("lock ip %s failed %v", ip.Name, err)
	}
	defer client.AlaudaV1().Locks().Delete(ip.Name, &metav1.DeleteOptions{})

	d, err := driver.GetIPAMDriver(alloc.Conf.Driver, alloc.Conf)
	if err != nil {
		return err
	}
	if err := d.AssignPrivateAddress(ip.Name); err != nil {
		return err
	}

	hostname, _ := os.Hostname()
	if ip.Labels == nil {
		ip.Labels = make(map[string]string)
	}
	ip.Labels["used"] = "true"
	ip.Spec.Used = true
	ip.Spec.Fresh = false
	ip.Spec.ServiceID = serviceID
	ip.Spec.ServiceName = serviceName
	ip.Spec.ControlledBy = controlledBy
	ip.Spec.PodName = alloc.Conf.PodName
	ip.Spec.Namespace = alloc.Conf.PodNamespace
	ip.Spec.NodeName = hostname
	ip.Spec.ContainerID = alloc.Conf.ContainerID
	newIP, err := client.AlaudaV1().IPs().Update(&ip)
	if k8s_error.IsConflict(err) {
		return errors.New("conflict")
	}
	if err != nil {
		return fmt.Errorf("update ip %s failed %v, %s", ip.Name, err, ip.ObjectMeta.ResourceVersion)
	}
	alloc.Logger.Printf("update ip %s, new: %+v\n", ip.Name, newIP)
	return nil
}

func (alloc *IPAllocator) release(client *versioned.Clientset, ip v1.IP, force bool) error {
	_, err := client.AlaudaV1().Locks().Create(&v1.Lock{ObjectMeta: metav1.ObjectMeta{Name: ip.Name}, Spec: v1.LockSpec{Owner: fmt.Sprintf("%s-%s", ip.Spec.Namespace, ip.Spec.PodName)}})
	if err != nil {
		return err
	}
	defer client.AlaudaV1().Locks().Delete(ip.Name, &metav1.DeleteOptions{})
	d, err := driver.GetIPAMDriver(alloc.Conf.Driver, alloc.Conf)
	if err != nil {
		return err
	}
	if err := d.UnassignPrivateAddress(ip.Name); err != nil {
		return err
	}

	if force {
		ip.Spec.ServiceID = ""
		ip.Spec.ServiceName = ""
		ip.Spec.ControlledBy = ""
		ip.Spec.Namespace = ""
		ip.Spec.Fresh = true
	}

	if ip.Labels == nil {
		ip.Labels = make(map[string]string)
	}
	ip.Labels["used"] = "false"
	ip.Spec.Used = false
	ip.Spec.PodName = ""
	ip.Spec.NodeName = ""
	ip.Spec.ContainerID = ""
	_, err = client.AlaudaV1().IPs().Update(&ip)
	return err
}

func (alloc *IPAllocator) Release() error {
	alaudaClient := config.GetAlaudaClient()
	allIPs, err := alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
	if err != nil {
		return err
	}

	pod, err := getPod(alloc.Conf.PodName, alloc.Conf.PodNamespace)
	if err != nil {
		return err
	}

	requiredIPs := getPodRequiredIPs(pod)
	for _, ip := range allIPs.Items {
		if ip.Spec.PodName == alloc.Conf.PodName && ip.Spec.Namespace == alloc.Conf.PodNamespace && ip.Spec.ContainerID == alloc.Conf.ContainerID {
			isRequired := false
			for _, requiredIP := range requiredIPs {
				if requiredIP == ip.Name {
					isRequired = true
					break
				}
			}
			if isRequired {
				// is a pod specific ip, keep service info
				err = alloc.release(alaudaClient, ip, false)
			} else {
				err = alloc.release(alaudaClient, ip, true)
			}
			alloc.Logger.Printf("pod %s, release ip %s, err: %v\n", alloc.Conf.PodName, ip.Name, err)
			return err
		}
	}

	// if no ip related to this pod, can safely return nil
	return nil
}

func (alloc *IPAllocator) gc() error {
	alaudaClient := config.GetAlaudaClient()
	allLocks, err := alaudaClient.AlaudaV1().Locks().List(metav1.ListOptions{})
	if err != nil {
		return fmt.Errorf("gc failed when list lock %v", err)
	}
	for _, lock := range allLocks.Items {
		if lock.CreationTimestamp.Add(50 * time.Second).Before(time.Now()) {
			err := alaudaClient.AlaudaV1().Locks().Delete(lock.Name, &metav1.DeleteOptions{})
			if err != nil {
				return fmt.Errorf("gc lock %s failed %v", lock.Name, err)
			}
		}
	}
	k8sClient := config.GetK8sClient()
	allIPs, err := alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
	if err != nil {
		return fmt.Errorf("gc failed when list ips %v", err)
	}
	for _, ip := range allIPs.Items {
		if ip.Spec.Used == true {
			_, err := k8sClient.CoreV1().Pods(ip.Spec.Namespace).Get(ip.Spec.PodName, metav1.GetOptions{})
			if k8s_error.IsNotFound(err) {
				err := alloc.release(alaudaClient, ip, true)
				if err != nil {
					return fmt.Errorf("gc %s failed %v", ip.Name, err)
				}
			}
		}
	}
	return nil
}

func findPodRequiredAddresses(pod *k8v1.Pod, logger *log.Logger) ([]string, error) {
	alaudaClient := config.GetAlaudaClient()

	subnetName := getPodRequiredSubnet(pod)
	specificIPs := getPodRequiredIPs(pod)
	podControlledBy := getPodControlledBy(pod)
	logger.Printf("pod: %s, subnet annotation: %s, ip annotation: %+v, controlledBy: %s\n", pod.Name, subnetName, specificIPs, podControlledBy)

	ipSet := set.New()
	allIPs, err := alaudaClient.AlaudaV1().IPs().List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	ipDict := map[string]v1.IP{}
	for _, ip := range allIPs.Items {
		ipDict[ip.Name] = ip
	}
	if subnetName != "" && len(specificIPs) > 0 {
		for _, ip := range allIPs.Items {
			if ip.Spec.SubnetName == subnetName && ip.Spec.Used == false {
				for _, sip := range specificIPs {
					if sip == ip.Name {
						ipSet.Add(ip.Name)
						break
					}
				}
			}
		}
	} else if subnetName != "" {
		// alloc by user specific subnet
		for _, ip := range allIPs.Items {
			if ip.Spec.SubnetName == subnetName && ip.Spec.Used == false {
				ipSet.Add(ip.Name)
			}
		}
	} else if len(specificIPs) > 0 {
		// alloc by user specific ips
		for _, ip := range allIPs.Items {
			if ip.Spec.Used == false {
				for _, sip := range specificIPs {
					if sip == ip.Name {
						ipSet.Add(ip.Name)
						break
					}
				}
			}
		}
	}
	// tweak ip order, binded first
	ipSlice := []string{}
	for _, ip := range set.StringSlice(ipSet) {
		if v1IP, found := ipDict[ip]; found {
			if v1IP.Spec.ControlledBy == podControlledBy {
				ipSlice = append(ipSlice, ip)
			} else if v1IP.Spec.ControlledBy == "" {
				ipSlice = append([]string{ip}, ipSlice...)
			}
		}
	}
	newIPSlice := make([]string, 0, len(ipSlice))
	// tweak ip order, used first
	for _, ip := range ipSlice {
		if v1IP, found := ipDict[ip]; found {
			if v1IP.Spec.Fresh == true {
				newIPSlice = append(newIPSlice, ip)
			} else {
				newIPSlice = append([]string{ip}, newIPSlice...)
			}
		}
	}

	if subnetName != "" || len(specificIPs) > 0 {
		if len(newIPSlice) == 0 {
			return nil, errors.New("no required ips available")
		}
	}
	return newIPSlice, nil
}

func getSubnet(subnetName string) (*v1.Subnet, error) {
	alaudaClient := config.GetAlaudaClient()
	subnet, err := alaudaClient.AlaudaV1().Subnets().Get(subnetName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return subnet, nil
}

func getPod(podName, podNamespaces string) (*k8v1.Pod, error) {
	k8sClient := config.GetK8sClient()
	pod, err := k8sClient.CoreV1().Pods(podNamespaces).Get(podName, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return pod, nil
}

func getPodControlledBy(pod *k8v1.Pod) string {
	k8sClient := config.GetK8sClient()
	ownerRefs := pod.GetOwnerReferences()
	if ownerRefs != nil && len(ownerRefs) > 0 {
		if ownerRefs[0].Kind == "ReplicaSet" {
			rs, err := k8sClient.AppsV1beta2().ReplicaSets(pod.Namespace).Get(ownerRefs[0].Name, metav1.GetOptions{})
			if err != nil {
				return ""
			}
			rsOwnerRefs := rs.GetOwnerReferences()
			if rsOwnerRefs != nil && len(rsOwnerRefs) > 0 {
				return rsOwnerRefs[0].Kind + "/" + rsOwnerRefs[0].Name
			}
		}
		return ownerRefs[0].Kind + "/" + ownerRefs[0].Name
	}
	return ""
}

func getPodRequiredIPs(pod *k8v1.Pod) []string {
	ips, hasIPs := pod.Annotations[RequiredAddresses]
	if !hasIPs {
		return nil
	}
	s := set.New()
	for _, ip := range strings.Split(ips, ",") {
		s.Add(strings.TrimSpace(ip))
	}
	return set.StringSlice(s)
}

func getPodRequiredSubnet(pod *k8v1.Pod) string {
	subnetName, hasSubnet := pod.Annotations[RequiredSubnet]
	if !hasSubnet {
		return ""
	}
	return strings.TrimSpace(subnetName)
}
